# Changelog

## 2.0.5 - 2024-06-09

### Fixed

- Compatible SPIP 4++
## 2.0.4 - 2023-06-02

### Fixed

- #10 `create_function()` n'existe plus en PHP8+ (cas du multidomaine)
## 2.0.3 - 2023-02-25

### Fixed

- Compatible SPIP 4.2

## 2.0.2 - 2022-12-14

### Fixed

- #9 Compatibilité Formidable 5.2.0
- Compatibilité PHP 8.1
- Correction d'erreur fatal avec les plugins crayons et identifiants
